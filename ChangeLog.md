# Revision history for machines-amazonka

## 0.5.0 -- 2017-04-12

* Update for [liblawless 0.19](http://hackage.haskell.org/package/liblawless-0.19.3)
* Add [Code of Conduct](http://contributor-covenant.org/version/1/4/code_of_conduct.md)
* Add a brief (very) [README.md](README.md)

## 0.4.0 -- 2017-02-17

* First version. Released on an unsuspecting world.
* Handles AMIs, some instance control, and spot price history.
