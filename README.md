# Amazonka Machines

This is a set
of [machines](http://hackage.haskell.org/package/machines)
and
[concurrent-machines](http://hackage.haskell.org/package/concurrent-machines) for
some of the components of the
various [Amazonka](http://hackage.haskell.org/package/amazonka)
libraries. They're composable, handle pagination, and transduction of
results with the full set of Machines primitives.

## Example Code

The Spot Price History module has one of the more complete examples,
including multiple concurrent requests, concurrent STM updates, and so
on.
