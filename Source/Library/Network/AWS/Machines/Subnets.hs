{-# LANGUAGE TemplateHaskell #-}

{-|
Module:             Network.AWS.Machines.Subnets
Description:        Transduces the DescribeSubnets results.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Network.AWS.Machines.Subnets where

import Network.AWS.EC2
import Network.AWS.Machines.AWS

subnets ∷ [RequestMod DescribeSubnets] → AWSSourceT m Subnet
subnets ms = awsSource dsrsSubnets ms describeSubnets
