{-|
Module:             Network.AWS.Machines.Instances
Description:        Machines producing instances
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <>
Stability:          experimental
Portability:        POSIX
-}

module Network.AWS.Machines.Instances where

import Lawless hiding (mapping)
import Machine
import Network.AWS.Machines.AWS
import Network.AWS.Machines.Types hiding (Instance)
import Network.AWS.EC2

reservations ∷ [RequestMod DescribeInstances] → AWSSourceT m Reservation
reservations ms = pagedSource dirsReservations ms describeInstances

instances ∷  AWSProcessT m Reservation Instance
instances = mapping (view rInstances) ⇝ asParts

stopT ∷ (MonadThrow m, M m, MonadIO m) ⇒ AWSProcessT m Instance InstanceStateChange
stopT = buffered 50 ~> autoM stop' ⇝ asParts
    where
        srq iids = stopInstances
                   & siInstanceIds
                   <>~ over traversed (view insInstanceId)  iids
        stop' iids = (send $ srq iids) >>= return ∘ view sirsStoppingInstances
