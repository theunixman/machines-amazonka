{-|
Module:             Network.AWS.Machines.
Description:        Machines producing instances
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Network.AWS.Machines.SecurityGroups where

import Network.AWS.Machines.AWS
import Network.AWS.EC2

securityGroups ∷ [RequestMod DescribeSecurityGroups] → AWSSourceT m SecurityGroup
securityGroups ms = awsSource dsgrsSecurityGroups ms describeSecurityGroups
