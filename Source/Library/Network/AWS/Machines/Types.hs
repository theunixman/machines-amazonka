{-|
Module:             Network.AWS.Machines.AWS.Types
Description:        Type reexports from Network.AWS.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Network.AWS.Machines.Types (
    module Network.AWS,
    module Control.Monad.Trans.AWS,
    module Control.Monad.IO.Class,
    module Control.Monad.Trans.Control,
    module Control.Monad.Catch,
    module Control.Monad.Trans.Resource,
    module Network.AWS.Types,
    module Network.AWS.AutoScaling.Types,
    M,
    AWSMachineT,
    AWSProcessT,
    AWSSourceT,
    GetResponse,
    RequestMod,
    IO
    ) where

import Lawless
import Network.AWS (MonadAWS, liftAWS, )
import Control.Monad.Trans.AWS
import Control.Monad.IO.Class
import Control.Monad.Trans.Control
import Control.Monad.Catch
import Control.Monad.Trans.Resource
import Network.AWS.Types
import Network.AWS.AutoScaling.Types
import Machine

-- | The base monad constraints for an 'AWSMachineT'.
type M m = (MonadBaseControl IO m, MonadAWS (AWST' Env (ResourceT m)))

-- | Any 'MachineT' operating in the AWST' stack.
type AWSMachineT m k b = M m ⇒ MachineT (AWST' Env (ResourceT m)) k b

-- | A process transducing a stream of AWS values.
type AWSProcessT m a b = AWSMachineT m (Is a) b

-- | A stream of AWS values.
type AWSSourceT m b = ∀ k. AWSMachineT m k b

-- | A 'Getter' for extracting values from an 'Rs' 'AWSRequest'.
type GetResponse f a c =
    (AWSRequest a, Foldable f) ⇒ Getting (f c) (Rs a) (f c)

-- | Modifies an 'AWSRequest' after the smart constructor has created
-- it.
type RequestMod a = a → a
