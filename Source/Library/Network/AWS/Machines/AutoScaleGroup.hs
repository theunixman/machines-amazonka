{-|
Module:             Network.AWS.Machines.LaunchConfigurations
Description:        Pull launch configuration information from AWS.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Network.AWS.Machines.AutoScaleGroup (
    autoScalingGroups,
    AutoScalingGroup) where

import Network.AWS.Machines.AWS
import Network.AWS.AutoScaling

autoScalingGroups ∷
    [RequestMod DescribeAutoScalingGroups] → AWSSourceT m AutoScalingGroup
autoScalingGroups ms = pagedSource dasgrsAutoScalingGroups ms describeAutoScalingGroups

-- -- | Create or update tags on a batch of 'AutoScaleGroup's,
-- -- yielding the launch configurations.
-- createOrUpdateTags ∷
--     [Tag] → AWSProcessT m AutoScalingGroup LaunchConfiguration
-- createOrUpdateTags tags = repeatedly $ do
--     lc ← await
--     let tgs = over traversed (\t → t & tagResourceId .~ lc ^. )
--     await ≫= coutTags (over traversed (flip set ))
