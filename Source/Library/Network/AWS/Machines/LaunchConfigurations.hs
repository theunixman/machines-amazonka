{-|
Module:             Network.AWS.Machines.LaunchConfigurations
Description:        Pull launch configuration information from AWS.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Network.AWS.Machines.LaunchConfigurations (
    module Network.AWS.AutoScaling.CreateLaunchConfiguration,
    module Network.AWS.AutoScaling.DescribeLaunchConfigurations,
    launchConfigurations
    ) where

import Network.AWS.Machines.AWS
import Network.AWS.Machines.Types hiding (await)
import Network.AWS.AutoScaling.CreateLaunchConfiguration
import Network.AWS.AutoScaling.DescribeLaunchConfigurations

launchConfigurations ∷
    [RequestMod DescribeLaunchConfigurations] → AWSSourceT m LaunchConfiguration
launchConfigurations ms =
    pagedSource dlcrsLaunchConfigurations ms describeLaunchConfigurations

-- -- | Create a new launch configuration for each
-- -- 'CreateLaunchConfiguration' that comes through. Return the actual
-- -- created 'LaunchConfiguration' by searching for the name.
-- createLaunchConfiguration ∷
--     [CreateLaunchConfiguration] →
--     AWSProcessT m CreateLaunchConfiguration LaunchConfiguration
-- createLaunchConfiguration =
--     let
--         c = repeatedly $ do
--             rq ← await
--             send  rq
--             yield $ rq ^. lcLaunchConfigurationName
--         bc = buffered 100
--         lcs = repeatedly $
--             await
--             ≫= launchConfigurations ∘ over traversed (flip set dlcLaunchConfigurationNames)
--             ≫= yield
--     in
--         c ⇝ bc ⇝ asParts ⇝ lcs
