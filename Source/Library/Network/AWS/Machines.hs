{-|
Module:             Network.AWS.Machines
Description:        Machines transducers for Amazonka functions.
Copyright:          © 2017 All rights reserved.
License:            GPL-3
Maintainer:         Evan Cofsky <evan@theunixman.com>
Stability:          experimental
Portability:        POSIX
-}

module Network.AWS.Machines (
    module Network.AWS.Machines.AWS,
    module Network.AWS.Machines.AutoScaleGroup,
    module Network.AWS.Machines.Filters,
    module Network.AWS.Machines.Images,
    module Network.AWS.Machines.Instances,
    module Network.AWS.Machines.LaunchConfigurations,
    module Network.AWS.Machines.SecurityGroups,
    module Network.AWS.Machines.SpotPrices,
    module Network.AWS.Machines.Subnets,
    module Network.AWS.Machines.Types
    ) where

import Network.AWS.Machines.AWS
import Network.AWS.Machines.AutoScaleGroup
import Network.AWS.Machines.Filters
import Network.AWS.Machines.Images
import Network.AWS.Machines.Instances
import Network.AWS.Machines.LaunchConfigurations
import Network.AWS.Machines.SecurityGroups
import Network.AWS.Machines.SpotPrices
import Network.AWS.Machines.Subnets
import Network.AWS.Machines.Types
